var media = "(prefers-reduced-motion: no-preference)",
  pref = window.matchMedia(media),
  showAnimations = !1,
  scrollSpeed = (showAnimations =
    pref.media == media && pref.matches ? !0 : showAnimations)
    ? 2e3
    : 0,
  fadeSpeed = showAnimations ? 1e3 : 0,
  transitionSpeed = showAnimations ? 300 : 0,
  transitionSpeedSlow = showAnimations ? 500 : 0,
  carouselSpeed = showAnimations ? 6e3 : 0;
